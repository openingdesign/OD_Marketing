Let's try something different.

Although [OpeningDesign](http://openingdesign.com/) is a talented bunch of [folks](http://openingdesign.com/people/) we could use your help!

Especially if you have the following skills and passions...

-   Having practiced for ± 15 years, you have mad chops in designing and producing 'blueprints'.  Yeah I said it--blueprints.
-   You have a quirky passion for technology, and love the process behind it--and dare I say, even more than design. Although you probably think design and process really can't be separated. 
-   You love remote working. In fact, you think it's more productive than face to face meetings and maybe even phone calls.
-   You're probably somewhat a sole-proprietor that has their own thing going on, or thinking about it. Cool! Can we work with you? OpeningDesign is like a loose group of moonlighters anyway.
-   You know your way around the IBC and IRC. Probably because you have to--you're a sole proprietor, after all!
-   You have a love/hate relationship with Revit, but would never go back to CAD again! In fact, because you love technology so much, you've dabbled in open source tools like [Blender](https://www.blender.org/) and [FreeCAD](https://www.freecadweb.org/). Or at least you want to!  That's what we're here for!  We've [just started](https://gitlab.com/openingdesign/heck_residence/-/tree/master/Models%20and%20CAD) producing blueprints with a Blender/FreeCAD workflow.
-   You love Pinterest, and proud of it. And actually dig [this](https://www.pinterest.com/theoryshaw/-architecture/) type of architecture.  In fact, you probably just pinned something just now.... thanks.
- You have somewhat of an opening in your schedule in the next couple months and can take on 1 or more of the following projects. 
	- A residential project in Atlanta, GA--currently early in SD phase. 
		- Files live [here](https://gitlab.com/openingdesign/223_randolph_st__atlanta)
		- [Here's](https://www.pinterest.com/theoryshaw/ag/) the client's aesthetic
		- They would love to use [ICF's](https://www.google.com/search?q=insulated+Concrete+Forms&safe=active&rlz=1C1FKPE_enUS928US928&sxsrf=ALeKk00IpVpvyM6JfZowCyOrgjIFTDE5zg%3A1623175284360&ei=dLC_YNuvFcGStAan15yQBQ&oq=insulated+Concrete+Forms&gs_lcp=Cgdnd3Mtd2l6EAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsAMyBwgAEEcQsANQAFgAYNnvBWgBcAJ4AIABhwGIAYcBkgEDMC4xmAEAqgEHZ3dzLXdpesgBCMABAQ&sclient=gws-wiz&ved=0ahUKEwjbue2Rz4jxAhVBCc0KHacrB1IQ4dUDCA4&uact=5)
	- A lakeside residential project on [Lake Kegonsa](https://www.google.com/maps/search/lake+kegonsa/@42.9721776,-89.285019,13z/data=!3m1!4b1)--currently in early SD phase
		- Files will live [here](https://gitlab.com/openingdesign/2154_colladay_dr)
		- [Here's](https://www.dropbox.com/sh/hpr28lz10q7uxd4/AACSUYTZwZzT2oGfbu0pVDHfa?dl=0) the clinic's aesthetic
	- A restaurant TI for [Forge Kitchen](https://www.foragemadison.com/)--currently very early SD
- And finally, you live somewhere on planet earth.

 
Did I mention we love technology?  Well we do, so here's the deal, for these projects, we'll dictate the technology pipeline, if you can run with the design and manage the project.   Don't worry, we have some [peeps](http://openingdesign.com/people/) to help you.

Did I also mention, that most of OpeningDesign's projects are open source and done [out-in-the-open](http://openingdesign.com/about/)?  Most of them live [here](https://gitlab.com/openingdesign) and [here](https://github.com/OpeningDesign) and look like [this](http://openingdesign.com/portfolio/). 

We're especially proud of our open source Revit [template](https://gitlab.com/openingdesign/OD_Library/-/blob/master/BIM/OD_Revit_Template%20-%202020.rte)--if you care to take it for a spin.

If such a thing interests you and think you have the chops. Please connect with me on the chat app, [Element.io](https://element.io/).  Just search for ```@theoryshaw:matrix.org```, that's my handle!   [Element.io](https://element.io/) is both our office and social water cooler.... stop by and say hi. 

Peace.

Ryan



<!--stackedit_data:
eyJoaXN0b3J5IjpbMjEyNjc5NTMyNSwtMTQ4MTQ5ODc0Nyw3OD
c5OTIyMiwxMjE3NTMyODg2LDE3NjA1NzY2MTMsMTY1MTY0NTky
Nyw5MTQ4NzYyODcsLTIwODUyMTc4OSwxOTc1MTI1MzQzLC0xMz
Y5NDQ5OTIyXX0=
-->